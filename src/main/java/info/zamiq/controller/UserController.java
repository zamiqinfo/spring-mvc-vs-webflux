package info.zamiq.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalDateTime;

@RestController
public class UserController {
    @GetMapping("/user")
    public Mono<String> getUserWithDelay(@RequestParam long delay) {
        return Mono.just(" ------ EXTERNAL SERVER | time on the external "+ LocalDateTime.now()).delayElement(Duration.ofMillis(delay));
    }
}
